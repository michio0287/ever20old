//
//  Popup.swift
//  ever20old
//
//  Created by kawase yu on 2015/03/19.
//  Copyright (c) 2015年 Michio Wakayama. All rights reserved.
//

import UIKit

class Popup: NSObject {

    private let contentView = UIView(frame: CGRectMake(0, 20, 320, AppUtil.windowHeight()))
    private let blackLayer = UIView(frame: CGRectMake(0, 0, 320, AppUtil.windowHeight()))
    private let yearLabel:UILabel = UILabel(frame: CGRectMake(0, 20, 320, 40))
    private let toggleButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton;
    private var parentView:UIView!
    private let overLine:UIView = UIView()
    private let underLine:UIView = UIView()
    
    private let nameLabel = UILabel()
    private let contentLabel = UILabel()
    private let popscrollView = UIScrollView()
    private var y:CGFloat = 0
    
    func setup(parentView:UIView){
        self.parentView = parentView
       
        // 背景
        blackLayer.backgroundColor = UIColor.blackColor()
        blackLayer.alpha = 0
        
        contentView.transform = CGAffineTransformMakeTranslation(0, AppUtil.windowHeight())
        
        // TODO nameLabelの大きさとかフォントとか
        contentView.addSubview(nameLabel)
        nameLabel.frame = CGRectMake(20, 70, 280, 40)
//        nameLabel.backgroundColor = UIColor.blueColor()
        nameLabel.textAlignment = NSTextAlignment.Center
        nameLabel.font = UIFont.systemFontOfSize(30.0)
        nameLabel.textColor = UIColor.whiteColor()
        nameLabel.adjustsFontSizeToFitWidth = true

        
        //contentLabelの大きさとかフォントとか
//        contentView.addSubview(contentLabel)
        contentLabel.numberOfLines = 0
//        contentLabel.backgroundColor = UIColor.blueColor()
        contentLabel.textAlignment = NSTextAlignment.Left
        contentLabel.font = UIFont.systemFontOfSize(20.0)
        contentLabel.textColor = UIColor.whiteColor()
        
    
        // ボタン
        toggleButton.frame = CGRectMake(0, 0, 320, 60)
        toggleButton.setImage(UIImage(named: "incidentButton"), forState: UIControlState.Normal)
//        toggleButton.setImage(UIImage(named: "incidentButtonClose"), forState: UIControlState.Selected)
        toggleButton.addTarget(self, action: "tapIncident:", forControlEvents: UIControlEvents.TouchUpInside)
        toggleButton.selected = false
        toggleButton.adjustsImageWhenHighlighted = false
        let whiteBg = UIView(frame: CGRectMake(0, 59.5, 320, AppUtil.windowHeight()-59.5))
        whiteBg.backgroundColor = UIColor.whiteColor()
        whiteBg.alpha = 0.2
        contentView.addSubview(whiteBg)
        
        yearLabel.textAlignment = NSTextAlignment.Center
        yearLabel.font = UIFont.systemFontOfSize(20.0)
        yearLabel.textColor = UIColor.whiteColor()
        toggleButton.addSubview(yearLabel)
        
        contentView.addSubview(toggleButton)
        
        // line
        overLine.backgroundColor = UIColorFromHex(0xffffff)
        contentView.addSubview(overLine)
        
        underLine.backgroundColor = UIColorFromHex(0xffffff)
        contentView.addSubview(underLine)
        
        //POP内のスクロール
        contentView.addSubview(popscrollView)
        popscrollView.addSubview(contentLabel)

    }
    
    func tapIncident(target:UIButton){
        target.selected = !target.selected
        
        if target.selected{
            // show
            show()
        }else{
            // hide
            hide()
        }
    }
    
    private func show(){
        parentView.addSubview(blackLayer)
        parentView.addSubview(contentView)
        AloeTweenChain().add(0.3, ease: AloeEase.OutCirc) { (val) -> () in
            self.blackLayer.alpha = val * 0.9
            let current:CGFloat = AppUtil.windowHeight() - 80
            let y:CGFloat = current - (current*val)
            self.contentView.transform = CGAffineTransformMakeTranslation(0, y)
        }.call { () -> () in
            self.toggleButton.setImage(UIImage(named: "incidentButtonClose"), forState: UIControlState.Normal)
        }.execute()
    }
    
    private func hide(){
        AloeTweenChain().add(0.3, ease: AloeEase.InCirc) { (val) -> () in
            self.blackLayer.alpha = 0.9 - (0.9*val)
            let y:CGFloat = (AppUtil.windowHeight() - 80)*val
            self.contentView.transform = CGAffineTransformMakeTranslation(0, y)
        }.call { () -> () in
            self.blackLayer.removeFromSuperview()
//            self.contentView.removeFromSuperview()
            self.toggleButton.setImage(UIImage(named: "incidentButton"), forState: UIControlState.Normal)
            
            // 広告を表示します
            println("showBySpotID")
            ImobileSdkAds.showBySpotID(IMOBILE_INTERSTITIAL_SID)
        }.execute()
    }
    
    // MARK: public
    
    func buttonShow(year:Int, name:String, content:String){
        yearLabel.text = String("\(year)歳の出来事")
        nameLabel.text = name
        
        y = 0
        if AppUtil.isTibi(){
            y = CGFloat(AppUtil.windowHeight()*120/568)
        }else{
            y = 120
        }
        contentLabel.frame = CGRectMake(0, 20, 280, 10000)
        contentLabel.text = content
        contentLabel.sizeToFit()
        popscrollView.frame = CGRectMake(20, y, 280, AppUtil.windowHeight()-y-40)
        popscrollView.contentSize = CGSizeMake(280, contentLabel.frame.size.height + 20 + 20/*padding*/)
        
        overLine.frame = CGRectMake(20, y, 280, 1)
        underLine.frame = CGRectMake(20, y + popscrollView.frame.size.height, 280, 1)
        
        parentView.addSubview(contentView)
        AloeTweenChain().add(0.4, ease: AloeEase.OutCirc) { (val) -> () in
            let y = AppUtil.windowHeight() - (80*val)
            self.contentView.transform = CGAffineTransformMakeTranslation(0, y)
        }.execute()
    }
    
    func staticHide(){

        contentView.transform = CGAffineTransformMakeTranslation(0, AppUtil.windowHeight())
    }

}
