//
//  ViewController.swift
//  ever20old
//
//  Created by 若山道雄 on 2015/03/16.
//  Copyright (c) 2015年 Michio Wakayama. All rights reserved.
//

import UIKit
import Social

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    private var yearList:[Int] = []
    private var monthList:[Int] = []
    private var dayList:[Int] = []
    private var ageList:[Int] = []
    private var isAge:Bool = false
    private let pickerView = UIPickerView()
    private let blackLayer = UIView(frame: CGRectMake(0, 0, 320, AppUtil.windowHeight()))
    private let label = UILabel()
    private let agelabel = UILabel()
    private let lastlabel = UILabel()
    private var ageIndex:Int = 0
    private var yearIndex:Int = 0
    private var monthIndex:Int = 0
    private var dayIndex:Int = 0
    private var comp: NSDateComponents!
    private let fbButton = UIImageView()
    private let twButton = UIImageView()
    private let targetYear:Int = 0
    private let targetIndex:Int = 0
    private let incident:NSDictionary!
    private var y:CGFloat = 0

    
    private let popup:Popup = Popup()
    
    private var incidentList:NSArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in 1900...2015{
            yearList.append(i)
        }
        
        for i in 1...12{
            monthList.append(i)
        }
        
        for i in 1...31{
            dayList.append(i)
        }
        for i in 0...29{
            ageList.append(i)
        }
        
        
        //アプリ全体のステータスバーの色を白に
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
        
        
        // 出来事リスト読み込み
        let path : String = NSBundle.mainBundle().pathForResource("incident", ofType: "json")!
        let fileHandle : NSFileHandle = NSFileHandle(forReadingAtPath: path)!
        let data : NSData = fileHandle.readDataToEndOfFile()
        incidentList = NSJSONSerialization.JSONObjectWithData(data,
            options: NSJSONReadingOptions.AllowFragments,
            error: nil) as NSArray
        
        for i in 0..<incidentList.count{
            let incident:NSDictionary = incidentList[i] as NSDictionary
        }
        
        // 背景を表示
        let imageView = UIImageView()
        imageView.image = UIImage(named: "bg")
        imageView.frame = CGRectMake(0, 0, 320, AppUtil.windowHeight())
        self.view.addSubview(imageView)

        // タイトルを表示
        let titeleView = UIImageView()
        titeleView.image = UIImage(named: "title")

        y = 0
        if AppUtil.isTibi(){
            y = CGFloat(AppUtil.windowHeight()*40/568)
        }else{
            y = 40
        }
        titeleView.frame = CGRectMake(30, y , 272, 35)
        self.view.addSubview(titeleView)

        // 誕生日フレームを表示
        let selectBgView = UIImageView()
        selectBgView.image = UIImage(named: "selectBg")

        y = 0
        if AppUtil.isTibi(){
            y = CGFloat(AppUtil.windowHeight()*105/568)
        }else{
            y = 105
        }
        selectBgView.frame = CGRectMake(30, y, 260, 45)
        selectBgView.userInteractionEnabled = true // タップを有効にする
        selectBgView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapBirthday:"))
        self.view.addSubview(selectBgView)

        // 誕生日を表示
        label.frame = CGRectMake(40, 0, 215, 45)
        label.textAlignment = NSTextAlignment.Left
        label.font = UIFont.systemFontOfSize(20.0)
        label.textColor = UIColor.grayColor()
        label.text = "誕生日"
        selectBgView.addSubview(label)
 
        // 誕生日ぴっかー
        pickerView.backgroundColor = UIColor.whiteColor()
        var frame = pickerView.frame;
        frame.origin.y = /*568*/ AppUtil.windowHeight()
        pickerView.frame = frame
        self.view.addSubview(pickerView)
        
        // ピッカービューの表示内容をだれが決めるかという指定
        pickerView.delegate = self
        pickerView.dataSource = self
        
        // 永遠の年齢フレームを表示
        let ageBgView = UIImageView()
        ageBgView.image = UIImage(named: "ageBg")

        y = 0
        if AppUtil.isTibi(){
            y = CGFloat(AppUtil.windowHeight()*177/568)
        }else{
            y = 177
        }
        ageBgView.frame = CGRectMake(30, y, 260, 45)
        ageBgView.userInteractionEnabled = true // タップを有効にする
        ageBgView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "agetapLabel:"))
        self.view.addSubview(ageBgView)
        
        // 永遠の年齢を表示
        agelabel.frame = CGRectMake(40, 0, 215, 45)
        agelabel.textAlignment = NSTextAlignment.Left
        agelabel.font = UIFont.systemFontOfSize(20.0)
        agelabel.textColor = UIColor.grayColor()
        agelabel.text = "永遠の◯◯歳"
        ageBgView.addSubview(agelabel)

        // 結果を表示
        y = 0
        if AppUtil.isTibi(){
            y = CGFloat(AppUtil.windowHeight()*348/568)
        }else{
            y = 348
        }
        lastlabel.frame = CGRectMake(30, y, 260, 60)
        lastlabel.textAlignment = NSTextAlignment.Center
        lastlabel.font = UIFont.systemFontOfSize(30.0)
        lastlabel.textColor = UIColor.whiteColor()
        lastlabel.text = ""
        self.view.addSubview(lastlabel)
        
        //黒いやつ表示
        blackLayer.backgroundColor = UIColor.blackColor()
        blackLayer.alpha = 0
        blackLayer.userInteractionEnabled = true // タップを有効にする
        blackLayer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapblackLayer"))
        
        // 計算フレームを表示
        let calcButton = UIImageView()
        calcButton.image = UIImage(named: "calcButton")

        y = 0
        if AppUtil.isTibi(){
            y = CGFloat(AppUtil.windowHeight()*253/568)
        }else{
            y = 253
        }
        calcButton.frame = CGRectMake(30, y, 260, 45)
        calcButton.userInteractionEnabled = true // タップを有効にする
        calcButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "calc:"))
        self.view.addSubview(calcButton)

        // facebookボタンを表示
        fbButton.image = UIImage(named: "fbButton")
        
        y = 0
        if AppUtil.isTibi(){
           y = CGFloat(AppUtil.windowHeight()*408/568)
        }else{
           y = 408
        }
        
        fbButton.frame = CGRectMake(75, y, 50, 50)
        fbButton.alpha = 0
        fbButton.userInteractionEnabled = true // タップを有効にする
        fbButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapfbButton:"))
        self.view.addSubview(fbButton)

        // twitterボタンを表示
        twButton.image = UIImage(named: "twButton")
        twButton.frame = CGRectMake(196, y, 50, 50)
        twButton.alpha = 0
        twButton.userInteractionEnabled = true // タップを有効にする
        twButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "taptwButton:"))
        self.view.addSubview(twButton)
        
        // popup
        popup.setup(self.view)
        
        
    }
    
    // 誕生日タップ時
    func tapBirthday(target:UITapGestureRecognizer){
        
        // ピッカーが表示されていたら閉じて終わる
        if isVisiblePicker(){
            hidePicker()
            return
        }
        
        
        isAge = false
         pickerView.reloadAllComponents()
        
        
        // TODO 同じように 3列分選択してやる
        pickerView.selectRow(yearIndex, inComponent: 0, animated: false)
        pickerView.selectRow(monthIndex, inComponent: 1, animated: false)
        pickerView.selectRow(dayIndex, inComponent: 2, animated: false)
        
        
        self.view.addSubview(blackLayer)
        self.view.addSubview(pickerView)
        
        // 今選択されているフォームを上に乗せる
        let targetView:UIView = target.view!
        self.view.addSubview(targetView)
        
        showPicker()
    }
    
    private func showPicker(){
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            
            var frame = self.pickerView.frame;
            frame.origin.y = AppUtil.windowHeight() - frame.size.height
            self.pickerView.frame = frame
            self.blackLayer.alpha = 0.7
            
            
            }) { (b) -> Void in
                
        }
    }
    
    private func hidePicker(){
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            var frame = self.pickerView.frame;
            frame.origin.y = AppUtil.windowHeight()
            self.pickerView.frame = frame
            self.blackLayer.alpha = 0
            
            }) { (b) -> Void in
                
        }
    }
    
    private func isVisiblePicker()->Bool{
        return (blackLayer.alpha != 0)
    }
    
    func agetapLabel(target:UITapGestureRecognizer){
        
        // ピッカーが表示されていたら閉じて終わる
        if isVisiblePicker(){
            hidePicker()
            return
        }
        
        isAge = true
        pickerView.reloadAllComponents()
        
        // ageIndex : 現在選択されてる年齢のインデックス
        pickerView.selectRow(ageIndex, inComponent: 0, animated: false)
        
        self.view.addSubview(blackLayer)
        self.view.addSubview(pickerView)
        
        // 今選択されているフォームを上に乗せる
        let targetView:UIView = target.view!
        self.view.addSubview(targetView)
        
        
        showPicker()
    }
    
    
    func tapblackLayer(){
        hidePicker()
        
        // 年齢の時
        if isAge{
            displayAge()
            return
        }
        
        // 以下、誕生日の時
        displayBirthday()
        
        
        //本日の日付を取得
        let now = NSDate()
        let df = NSDateFormatter()
        df.locale = NSLocale(localeIdentifier: "ja_JP")
        df.dateFormat = "yyyyMMdd"
        let str:String = df.stringFromDate(now)
        let today:Int = str.toInt()!
        
        
    }
    
    private func displayAge(){
        ageIndex = self.pickerView.selectedRowInComponent(0)
        agelabel.text = "\(self.ageList[ageIndex])歳"
        agelabel.textColor = UIColorFromHex(0x333333)
    }
    
    private func displayBirthday(){
        yearIndex = self.pickerView.selectedRowInComponent(0)
        monthIndex = self.pickerView.selectedRowInComponent(1)
        dayIndex = self.pickerView.selectedRowInComponent(2)
        label.text = "\(self.yearList[yearIndex])年\(self.monthList[monthIndex])月\(self.dayList[dayIndex])日"
        label.textColor = UIColorFromHex(0x333333)
    }
    
    func agetapblackLayer(){
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            
            var frame = self.pickerView.frame;
            frame.origin.y = AppUtil.windowHeight()
            self.pickerView.frame = frame
            self.blackLayer.alpha = 0
            

            }) { (b) -> Void in
                
                
                
        }
    }
    
    //計算
    func calc(recognizer:UITapGestureRecognizer){
        

        
        if label.text == "誕生日"{
            label.textColor = UIColorFromHex(0x990000)
            pain(label.superview!)
        }else if agelabel.text == "永遠の◯◯歳"{
            agelabel.textColor = UIColorFromHex(0x990000)
            pain(agelabel.superview!)
        }else{
            let targetView:UIView = recognizer.view!
            pain(targetView)
        let calendar = NSCalendar(identifier: NSGregorianCalendar)!
            
        let Birth:NSDate = calendar.dateWithEra(1, year: yearList[yearIndex], month: monthList[monthIndex], day: dayList[dayIndex], hour: 0, minute: 0, second: 0, nanosecond: 0)!
            
        let everBirth:NSDate = calendar.dateWithEra(1, year: yearList[yearIndex]+ageList[ageIndex], month: monthList[monthIndex], day: dayList[dayIndex], hour: 0, minute: 0, second: 0, nanosecond: 0)!
        
        
        let todayY = NSDate()
        let second:NSTimeInterval = NSTimeInterval(NSTimeZone.defaultTimeZone().secondsFromGMTForDate(Birth))
        let eversecond:NSTimeInterval = NSTimeInterval(NSTimeZone.defaultTimeZone().secondsFromGMTForDate(everBirth))
        let BirthJa:NSDate = NSDate(timeInterval: second, sinceDate: Birth)
        let everBirthJa:NSDate = NSDate(timeInterval: eversecond, sinceDate: everBirth)
        

        comp = calendar.components(NSCalendarUnit.CalendarUnitDay, fromDate: everBirthJa, toDate: todayY, options:nil)
        lastlabel.text = "\(ageList[ageIndex])歳と\(comp.day)日"
//        incidentlabel.text = "\(yearList[yearIndex]+ageList[ageIndex])年の出来事"
            
        popup.staticHide()
            //計算結果のアニメーション
            lastlabel.alpha = 0
            lastlabel.transform = CGAffineTransformMakeScale(0, 0)
            
//            fbButton.transform = CGAffineTransformMakeScale(0, 0)
//            twButton.transform = CGAffineTransformMakeScale(0, 0)
            fbButton.alpha = 0
            twButton.alpha = 0
            fbButton.transform = CGAffineTransformMakeTranslation(0, 20)
            fbButton.transform = CGAffineTransformMakeTranslation(0, 20)
            
            
            AloeTweenChain().add(0.4, ease: AloeEase.OutBack, progress: { (val) -> () in
                self.lastlabel.transform = CGAffineTransformMakeScale(val, val)
                self.lastlabel.alpha = val
            }).wait(0.2).add(0.2, ease: AloeEase.OutCirc, progress: { (val) -> () in
                let y:CGFloat = 20 - (20*val)
                self.fbButton.transform = CGAffineTransformMakeTranslation(0, y)
                self.fbButton.alpha = val
                self.twButton.transform = CGAffineTransformMakeTranslation(0, y)
                self.twButton.alpha = val
            })
            .call({ () -> () in
                // TODO
                
                let targetYear:Int = self.yearList[self.yearIndex]+self.ageList[self.ageIndex]
                let targetIndex:Int = targetYear - 1900
                
                
                let incident:NSDictionary = self.incidentList[targetIndex] as NSDictionary
//                println("\(incident)")
                let name = incident["name"]as String
//                println("\(name)")
                let content = incident["content"]as String
//                println("\(content)")
                
                self.popup.buttonShow(self.ageList[self.ageIndex], name: "\(name)", content: "\(content)")


            })
            .execute()
            
        }
    }
    
    //出来事
    func tapincidentButton(){
//        let targetYear:Int = yearList[yearIndex]+ageList[ageIndex]
//        let targetIndex:Int = targetYear - 1900
        
//        let incident:NSDictionary = incidentList[targetIndex] as NSDictionary
//        println("\(incident)")
//        let name = incident["name"]as String
//        println("\(name)")
//        let content = incident["content"]as String
//        println("\(content)")
        
    }
    
    func tapfbButton(recognizer:UITapGestureRecognizer){
        let targetView:UIView = recognizer.view!
        pain(targetView)
        var controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        
        // リンク先
        let link: String = "https://itunes.apple.com/us/app/yong-yuanno-sui!/id978615781?l=ja&ls=1&mt=8"
        let url = NSURL(string: link)
        controller.addURL(url)
        
        // テキスト
        let title: String = "私の年齢は \(ageList[ageIndex])歳と\(comp.day)日 です！" // ここに結果の文言を入れる
        controller.setInitialText(title)
        
        // 画像
        //        let image = UIImage(named: "result5")
        //        controller.addImage(image)
        
        presentViewController(controller, animated: true, completion: {})
    }

    func taptwButton(recognizer:UITapGestureRecognizer){
        let targetView:UIView = recognizer.view!
        pain(targetView)
        
        var controller = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        
        // リンク先
//        https://itunes.apple.com/us/app/yong-yuanno-sui!/id978615781?l=ja&ls=1&mt=8
        let link: String = "https://itunes.apple.com/us/app/yong-yuanno-sui!/id978615781?l=ja&ls=1&mt=8"
        let url = NSURL(string: link)
        controller.addURL(url)
        
        // テキスト
        let title: String = "私の年齢は \(ageList[ageIndex])歳と\(comp.day)日 です！" // ここに結果の文言を入れる
        controller.setInitialText(title)
        
        // 画像
        //        let image = UIImage(named: "result5")
        //        controller.addImage(image)
        
        presentViewController(controller, animated: true, completion: {})
    }
    
    
    private func pain(view:UIView){
        view.transform = CGAffineTransformMakeScale(0.9, 0.9)
        AloeTween.doTween(0.3, ease: AloeEase.OutBack) { (val) -> () in
            let s = 0.9 + (0.1 * val)
            view.transform = CGAffineTransformMakeScale(s, s)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    // 誕生日ぴっかー何列表示するか
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        
        if isAge{
            return 1
        }
        
        return 3
    }
    

    
    // 誕生日ぴっかー何行表示するか
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if isAge{
            var borderYear = yearList[yearList.count-1]-(ageList.count-1)
            if yearList[yearIndex] >= borderYear {
                return (ageList.count - (yearList[yearIndex]-borderYear))
            }else{
                return ageList.count
            }

        }
        
        if component == 0{
            return yearList.count
        }else if component == 1{
            return monthList.count
        }else if component == 2{  // 日付が何行あるか
            // 選択されている月のインデックスを取ってくる
            let monthIndex = pickerView.selectedRowInComponent(1)
            let yearIndex = pickerView.selectedRowInComponent(0)
            
            //うるう年を判定
            switch(monthIndex){
            case 1:
                if yearList[yearIndex] % 4 == 0{
                    if yearList[yearIndex] % 400 == 0{
                        return 29
                    }else if yearList[yearIndex] % 100 == 0{
                        return 28
                    }else {
                        return 29
                    }
                }else {
                    return 28
                }
            case 3,5,8,10:
                return 30
            default:
                return 31
            }
        }
        //ここにはこないはず
        return 0
    }
    
    // 誕生日ぴっかーに表示する内容
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String!{
        
        if isAge{
            return String("\(ageList[row])")
            
        }
        
        switch(component){
        case 0:
            return String("\(yearList[row])")
        case 1:
            return String("\(monthList[row])")
        default:
            return String("\(dayList[row])")
        }
        
    }

    // ぴっかーが変更された時に呼ばれる
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if isAge{
            println("年齢が変更されたよ\(ageList[row]) 歳")
            // 変更されたタイミングで反映
            displayAge()
            return
        }
        
        println("選択されたのは:\(component)列目の \(row)行目")
        if component == 0 {
            pickerView.reloadAllComponents()
        }else if component == 1 {
            pickerView.reloadAllComponents()
        }
        
        // 変更されたタイミングで反映
        displayBirthday()

        
    }
    
}

