//
//  Igai.swift
//  ever20old
//
//  Created by 若山道雄 on 2015/03/17.
//  Copyright (c) 2015年 Michio Wakayama. All rights reserved.
//

import UIKit

class Igai: NSObject, UIPickerViewDelegate, UIPickerViewDataSource {
   
    
    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return 0
    }
}
