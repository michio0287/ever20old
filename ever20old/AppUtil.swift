//
//  AppUtil.swift
//  ever20old
//
//  Created by 若山道雄 on 2015/03/19.
//  Copyright (c) 2015年 Michio Wakayama. All rights reserved.
//

import UIKit


let IMOBILE_INTERSTITIAL_PID = "37871"
let IMOBILE_INTERSTITIAL_MID = "157980"
let IMOBILE_INTERSTITIAL_SID = "415713"


func UIColorFromHex(rgbValue:UInt32)->UIColor{
    let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
    let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
    let blue = CGFloat(rgbValue & 0xFF)/256.0
    
    return UIColor(red:red, green:green, blue:blue, alpha:1.0)
}

class AppUtil: NSObject {
    class func windowHeight()->CGFloat{
        return UIScreen.mainScreen().bounds.size.height;
    }
    
    class func isTibi()->Bool{
        return (self.windowHeight() < 568)
    }
}
